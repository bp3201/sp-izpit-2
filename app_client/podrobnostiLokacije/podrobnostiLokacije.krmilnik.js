(function() {
  function podrobnostiLokacijeCtrl($routeParams, $location, $uibModal, edugeocachePodatki, avtentikacija) {
    var vm = this;

    vm.idLokacije = $routeParams.idLokacije;
    
    vm.jePrijavljen = avtentikacija.jePrijavljen();
    
    vm.prvotnaStran = $location.path();
    
    
    if(avtentikacija.trenutniUporabnik() != undefined){
      vm.email = avtentikacija.trenutniUporabnik().elektronskiNaslov;
    }
   // console.log(vm.email);
    
    vm.brisanjeLastnegaKomentarja = function(lokacijaId, komentarId){
      edugeocachePodatki.brisiKomentarZaId(lokacijaId, komentarId).then(function success(response){
        alert("Uspešno ste obrisali komentar.");
      }, function error(response){
        if(response.status == 401){
          vm.napaka = "Komentar lahko izbriše le njegov avtor!"
        }else{
          vm.napaka = "Prišlo je do napake!"
        }
      });
      
      
      //da bi se strana takoj posodobila
      edugeocachePodatki.podrobnostiLokacijeZaId(vm.idLokacije).then(
      function success(odgovor) {
        vm.podatki = { lokacija: odgovor.data };
        vm.glavaStrani = {
          naslov: vm.podatki.lokacija.naziv
        };
      },
      function error(odgovor) {
        console.log(odgovor.e);
      }
    );
    };

    vm.prikaziPojavnoOknoObrazca = function() {
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/komentarModalnoOkno/komentarModalnoOkno.pogled.html',
        controller: 'komentarModalnoOkno',
        controllerAs: 'vm',
        resolve: {
          podrobnostiLokacije: function() {
            return {
              idLokacije: vm.idLokacije,
              nazivLokacije: vm.podatki.lokacija.naziv
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.podatki.lokacija.komentarji.push(podatki);
      }, function(napaka) {
        // Ulovi dogodek in ne naredi ničesar
      });
    };
    
    edugeocachePodatki.podrobnostiLokacijeZaId(vm.idLokacije).then(
      function success(odgovor) {
        vm.podatki = { lokacija: odgovor.data };
        vm.glavaStrani = {
          naslov: vm.podatki.lokacija.naziv
        };
      },
      function error(odgovor) {
        console.log(odgovor.e);
      }
    );
  }
  podrobnostiLokacijeCtrl.$inject = ['$routeParams', '$location', '$uibModal', 'edugeocachePodatki', 'avtentikacija'];
  
  /* global angular */
  angular
    .module('edugeocache')
    .controller('podrobnostiLokacijeCtrl', podrobnostiLokacijeCtrl);
})();