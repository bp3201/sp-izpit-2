(function() {
  var prikaziOceno = function() {
    return {
      restrict: 'EA',
      scope: {
        trenutnaOcena: "=ocena"
      },
      templateUrl: "/skupno/direktive/prikaziOceno/ocena-zvezdice.html"
    };
  };
  
  /* global angular */
  angular
    .module('edugeocache')
    .directive('prikaziOceno', prikaziOceno);
})();